#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
using namespace std;
using namespace sf;

int e;
int lines = 0;
int elements = 0;
double ee;
struct object
{
	virtual void f() {}
};

struct road : object
{
	virtual void f(){}
	Vertex* line;
	object* A;
	object* B;
};
struct elem : object
{
	virtual void f() {}
	elem(float x, float y);
	~elem();
	vector<elem*> friends;//����������
	vector<elem*> canBeFriends;
	vector<road*> roads;
	CircleShape* circle;
	int action;
	float x, y;
	void changeAction(object* manager);
	elem* createElem(object* manager);
	void createListLine();
	void createLine(object* manager);
	elem* deleteLine(object* manager);
	void destroySelf(object* manager);
};
/*
1. ��������� �����
2. ������� �����
3. ������� �������
4. ������� �������
*/
struct manage : object
{
	virtual void f() {}
	vector<road*> roads;
	vector<elem*> elems;
	manage();
	void tick();
};

int main()
{
	setlocale(LC_ALL, "");
	srand(time(0));
	cout << "������� �� ������ � ������� ���-�� ������: ";
	cin >> e;
	ee = sqrt(800.0 * 600 / 6 / 3.1415 / e);
	//ee = 5;
	manage manager;
	int time = 500;
	int i = 0;
	// Define some constants
	const int gameWidth = 800;
	const int gameHeight = 600;
	// Create the window of the application
	sf::RenderWindow window(sf::VideoMode(gameWidth, gameHeight, 32), L"SFML ����� v0.1",
		sf::Style::Titlebar | sf::Style::Close);
	window.setVerticalSyncEnabled(true);

	// Load the text font
	sf::Font font;
	if (!font.loadFromFile("resources/sansation.ttf"))
		return EXIT_FAILURE;

	// Initialize the pause message
	sf::Text pauseMessage;
	pauseMessage.setFont(font);
	pauseMessage.setCharacterSize(40);
	pauseMessage.setPosition(200.f, 150.f);
	pauseMessage.setFillColor(sf::Color::White);
	pauseMessage.setString("Welcome!\nPress space to start");

	
	bool isPlaying = false;
	while (window.isOpen())
	{
		// Handle events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Window closed or escape key pressed: exit
			if ((event.type == sf::Event::Closed) ||
				((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape)))
			{
				window.close();
				break;
			}

			// Space key pressed: play
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Space))
			{
				if (!isPlaying)
				{
					// (re)start the game
					isPlaying = true;

					// Reset
				}
			}
		}

		if (isPlaying)
		{
			manager.tick();
			if ((elements == 0 and lines == 0) or i>=time)
			{
				window.close();
				break;
			}

			// Move the player's paddle
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{

			}

			
			/*if (ball.getPosition().x - ballRadius < 0.f)
			{
				isPlaying = false;
				pauseMessage.setString("You lost!\nPress space to restart or\nescape to exit");
			}
			if (ball.getPosition().x + ballRadius > gameWidth)
			{
				isPlaying = false;
				pauseMessage.setString("You won!\nPress space to restart or\nescape to exit");
			}*/
			
			
			
		}

		// Clear the window
		window.clear(sf::Color(50, 200, 50));

		if (isPlaying)
		{
			// Draw the paddles and the ball
			for (int i = 0; i < manager.elems.size(); i++)
			{
				window.draw(*(manager.elems[i]->circle));
			}
			for (int i = 0; i < manager.roads.size(); i++)
			{
				window.draw(manager.roads[i]->line, 2, Lines);
			}
		}
		else
		{
			// Draw the pause message
			window.draw(pauseMessage);
		}

		// Display things on screen
		window.display();
	}

	return EXIT_SUCCESS;
}




elem::elem(float x, float y)
{
	this->x = x;
	this->y = y;
	circle = new CircleShape;
	circle->setRadius(ee);//~e
	//circle->setOutlineThickness(3);
	//circle->setOutlineColor(sf::Color::Black);
	circle->setFillColor(sf::Color::Red);
	circle->setOrigin(-x,-y);
}
elem::~elem()
{
	delete circle;
	for (int i = 0; !roads.empty(); i++)
	{
		cout << "ERROR::DELETED TOWN WITH ROADS";
		auto r=roads[roads.size()-1];
		delete r;
		roads.pop_back();
		lines--;
	}
}

void elem::changeAction(object* manager)
{
	destroySelf(manager);
	int a1, a2, a3;
	a1 = 10000; //create line
	a2 = 10000; //delete line
	a3 = 10000; // create elem
	double k = double(elements) / (elements + e)*0.5;
	a2 *= k;
	int a = rand() % (a1 + a2 + a3);
	if (a < a1)
		action = 1;
	else if (a < a1 + a2)
		action = 2;
	else if (a < a1 + a2 + a3)
		action = 3;
}

elem* elem::createElem(object* manager)
{
	double dx, dy;
	dx = (rand() % int(ee)) * 4- (rand() % int(ee))*4;
	dy = (rand() % int(ee)) * 4- (rand() % int(ee))*4;
	elem* a = new elem(x+dx,y+dy);
	friends.push_back(a);
	/*VertexArray* line = new VertexArray(Lines,2);
 	(*line)[0].position = Vector2f(x, y);
	(*line)[1].position = Vector2f(x+dx, y+dy);
	(*line)[0].color = Color::Blue;
	(*line)[1].color = Color::Blue;*/
	Vertex* line = new Vertex;
	line[0].position = Vector2f(x, y);
	line[1].position = Vector2f(x + dx, y + dy);
	line->color = Color::Blue;
	road* r = new road;
	r->A = this;
	r->B = a;
	r->line = line;
	roads.push_back(r);
	a->friends.push_back(this);
	a->roads.push_back(r);
	manage* m;
	m = dynamic_cast<manage*>(manager);
	m->elems.push_back(a);
	m->roads.push_back(r);
	elements++;
	lines++;
	return a;
}

bool find(elem* element, vector<elem*> arr)
{
	for (int i = 0; i < arr.size(); i++)
	{
		if (arr[i] == element)
		{
			return true;
		}
	}
	return false;
}

void elem::createListLine()
{
	canBeFriends.clear();
	for (int i = 0; i < friends.size(); i++)
	{
		for (int j = 0; j < friends[i]->friends.size(); j++)
		{
			elem* a = friends[i]->friends[j];
			if (!find(a, friends) && !find(a, canBeFriends) && this != a)
			{
				canBeFriends.push_back(a);
			}
		}
	}
}

void elem::createLine(object* manager)
{
	if (canBeFriends.empty())
		return;
	elem* b = canBeFriends[rand() % canBeFriends.size()];
	if (find(b, friends))
		return;
	
	friends.push_back(b);
	b->friends.push_back(this);
	lines++;
	Vertex* line = new Vertex;
	line[0].position = Vector2f(x, y);
	line[1].position = Vector2f(b->x, b->y);
	line->color = Color::Blue;
	road* r = new road;
	r->A = this;
	r->B = b;
	r->line = line;
	roads.push_back(r);
	b->roads.push_back(r);
	manage* m;
	m = dynamic_cast<manage*>(manager);
	m->roads.push_back(r);
}

elem* elem::deleteLine(object* manager)
{
	if (friends.size() == 0)
		return nullptr;
	int a = rand() % friends.size();
	elem* b = friends[a];
	friends.erase(friends.begin() + a);
	road* r=nullptr;
	for (int i = 0; i < roads.size(); i++)
	{
		if (roads[i]->A == b or roads[i]->B == b)
		{
			r = roads[i];
			roads.erase(roads.begin() + i);
		}
	}

	auto it = b->friends.begin();
	for (; (*it) != this; it++);
	b->friends.erase(it);

	for (int i = 0; i < b->roads.size(); i++)
	{
		if (b->roads[i]==r)
		{
			b->roads.erase(b->roads.begin() + i);
		}
	}

	manage* m;
	m = dynamic_cast<manage*>(manager);
	for (int i = 0; i < m->roads.size(); i++)
	{
		if (m->roads[i] == r)
		{
			m->roads.erase(m->roads.begin() + i);
		}
	}
	delete r;
	
	lines--;
	return b;
}

void elem::destroySelf(object* manager)
{
	manage* m;
	m = dynamic_cast<manage*>(manager);
	if (!friends.empty())
		return;
	for (int i = 0; i < m->elems.size(); i++)
	{
		if (m->elems[i] == this)
		{
			m->elems.erase(m->elems.begin() + i);
			break;
		}
	}
	for (int i = 0; i < friends.size(); i++)
	{
		for (int j = 0; j < friends[i]->friends.size(); j++)
		{
			if (friends[i]->friends[j] == this)
			{
				friends[i]->friends.erase(friends[i]->friends.begin() + j);
				break;
			}
		}
	}
	elements--;
	delete this;
	return;
}

manage::manage()
{
	elems.push_back(new elem(400,300));
	elements++;
	for (size_t i = 0; i < 3; i++)
	{
		elems[0]->createElem(this);
	}
}

int i = 0;
void manage::tick()
{
	int start = clock();
	int elements_1 = elements;
	for (int i = 0; i < elems.size(); i++)
	{
		elems[i]->changeAction(this);
	}
	for (int i = 0; i < elems.size(); i++)
	{
		if (elems[i]->action == 1)
			elems[i]->createListLine();
	}
	for (int i = 0; i < elems.size(); i++)
	{
		if (elems[i]->action == 2)
			elems[i]->deleteLine(this);
	}
	for (int i = 0; i < elems.size(); i++)
	{
		if (elems[i]->action == 1)
			elems[i]->createLine(this);
	}
	for (int i = 0; i < elems.size(); i++)
	{
		if (elems[i]->action == 3)
			elems[i]->createElem(this);
	}
	int end = clock();
	cout << i++ << ") " << "������ = " << lines << "\t������ = " << elements << "\t����� = " << end - start << "��"
		<< "\t� ����� ������� " << double(lines) / elements * 2 << " ������"
		<< "\t����������� �� " << double(elements) / elements_1 * 100 - 100 << "%" << endl;

}